use std::fmt::{Display,self};

#[cfg(test)]
mod tests;

use serde::{Serialize, Deserialize};

use chrono::prelude::*;

#[derive(Serialize, Deserialize, Debug,Clone)]
pub struct Date {
    time: chrono::DateTime<chrono::Local>
}
impl Date {
    pub fn now() -> Date {
        Date {
            time: Local::now()
        }
    }
    pub fn day(&self) -> String {
        self.time.format("%Y-%m-%d").to_string()
    }
}
impl Display for Date {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[ {} ]", self.time.format("%H:%M:%S"))
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq,Clone)]
pub enum Command {
    //Login with specified username
    Login(String),
    // Send a message
    Message(String),
    // Set what client we're using
    Client(String)
}
//Figure out what command we have from a string
impl From<&str> for Command {
    fn from(s: &str) -> Self {
        let mut tokens = s.split_whitespace();
        match tokens.next() {
            Some("/login") => Command::Login(tokens.collect()),
            Some("/client") => Command::Client(tokens.collect()),
            _ => Command::Message(s.to_owned())
        }
    }
}

#[derive(Serialize, Deserialize, Debug,Clone)]
pub enum Message {
    //Joins and Parts both have a username and time
    Join(String,Date),
    Part(String, Date),
    //Date change
    Date(Date),
    //Messages have a username, message body, and time
    Message(String,String,Date)
}
impl Message {
    pub fn join(s: String) -> Message {
        Message::Join(s,Date::now())
    }
    pub fn part(s: String) -> Message {
        Message::Part(s,Date::now())
    }
    pub fn date() -> Message {
        Message::Date(Date::now())
    }
    pub fn message(user: String,s: String) -> Message {
        Message::Message(user,s,Date::now())
    }
}
impl Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Message::*;
        match self {
            Join(name,date) => {
                write!(f, "{} {} has joined", date,name)
            },
            Part(name,date) => {
                write!(f, "{} {} has quit", date,name)
            },
            Date(date) => {
                write!(f, "It is now {}", date.day())
            },
            Message(name,body,date) => {
                write!(f, "{} {}: {}", date,name,body)
            }

        }
    }
}
