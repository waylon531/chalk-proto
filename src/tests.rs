mod from_str {
    use crate::Command;
    #[test]
    fn login() {
        let s = "/login testuser";
        assert_eq!(Command::Login("testuser".to_owned()),s.into())
    }
    #[test]
    fn message() {
        let s = "Hello World!";
        assert_eq!(Command::Message("Hello World!".to_owned()),s.into())
    }
}
